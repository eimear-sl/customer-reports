from analystutils import db_reader as db
import os
from analystutils import notifications

game_id = '7161'

sql = ''' with games as
  (select {0} as gameid)
  , shots_faced as
  (select gameid
  , opposingteamgoalieoniceid
  , sum(case when flags2json(flags)::text like '%%shotongoal%%' then 1 else 0 end) as shots_faced
  from playereventlocations_dw
  where gameid in (select gameid from games)
  group by 1,2)
  , scores as
  (
  select a.gameid, a.team, a.opponent, goals_for, goals_against
  from
  (select gameid
  , case when shorthand = 'OWN GOAL' then opposingteamshorthand else teamshorthand end as team
  , case when shorthand = 'OWN GOAL' then teamshorthand else opposingteamshorthand end as opponent
  , sum(case when eventname='goal' then 1 else 0 end) as goals_for
  from playereventlocations_dw
  where gameid in (select gameid from games)
  group by 1,2,3) a
  left join
  (select gameid
  , case when shorthand = 'OWN GOAL' then teamshorthand else opposingteamshorthand end as team
  , case when shorthand = 'OWN GOAL' then opposingteamshorthand else teamshorthand end as opponent
  , sum(case when eventname='goal' then 1 else 0 end) as goals_against
  from playereventlocations_dw
  where gameid in (select gameid from games)
  group by 1,2,3) b
  on a.gameid=b.gameid
  and a.team=b.team
  )
  , play_time as
  (
  select gameid, playerid
  , sum(coalesce(toiseconds, 0))/60 as minutes_played
  from playermetricscrosstab_dw
  where gameid in (select gameid from games)
  group by 1,2
  )
  , game_time as
  (
  select gameid
  , max(minutes_played) as gameduration
  from (select gameid, playerid
  , sum(coalesce(toiseconds, 0))/60 as minutes_played
  from playermetricscrosstab_dw
  where gameid in (select gameid from games)
  group by 1,2) pt
  group by 1
  )
  , subsequent_events as
  (select secondaryplayereventid, "zone", eventname
  from playereventlocations_dw
  where gameid in (select gameid from games) and
  eventname != 'press' and
  (eventname='save'
  or "zone"='off'))
  select a.gameid
  , a."date"
  , a.teamshorthand
  , a.opposingteamshorthand as opponent
  , gameduration
  , goals_for as teamgoals_scored
  , goals_against as teamgoals_conceded
  , a.playerid
  , concat(concat(a.playerfirstname, ' '),a.playerlastname) as playername
  , a.playerprimaryposition
  , minutes_played
  --passes
  , sum(case when a.eventname='pass' then 1 else 0 end) as total_passes
  , sum(case when a.eventname='pass' and a.outcome='successful' then 1 else 0 end) as successful_passes
  , sum(case when a.eventname='cross' then 1 else 0 end) as total_crosses
  , sum(case when a.eventname='cross' and a.outcome='successful' then 1 else 0 end) as successful_crosses
  , sum(case when flags2json(a.flags)::text like '%%line-break%%' then 1 else 0 end) as line_breaking_options
  , sum(case when flags2json(a.flags)::text like  '%%Line%%' then 1 else 0 end) as line_breaking_passes
  , sum(case when flags2json(a.flags)::text like  '%%Line%%' and a.outcome='successful' then 1 else 0 end) as successful_line_breaking_passes
  --defensive actions
  , sum(case when a.eventname='block' and a.outcome='successful' and flags2json(a.flags)::text not like '%%interception%%' then 1 else 0 end) as blocks
  , sum(case when a.eventname='block' and a.outcome='successful' and flags2json(a.flags)::text like '%%interception%%' then 1 else 0 end) as interceptions
  , sum(case when a.eventname='lbr' and a.outcome='successful' then 1 else 0 end) as LBRs
  , sum(case when a.eventname='tackle' then 1 else 0 end) as total_tackles
  , sum(case when a.eventname='tackle' and a.outcome='successful' then 1 else 0 end) as successful_tackles
  , sum(case when a.eventname='clearance' then 1 else 0 end) as total_clearances
  --chance creation
  , sum(case when a.eventname='carry' and a.outcome='successful' then 1 else 0 end) as successful_carries
  , sum(case when a.eventname='pass' and c."zone"='off' and a.outcome='successful' then 1 else 0 end) as final_third_passes
  , sum(case when a.eventname in ('pass', 'cross') and a.outcome='successful' and flags2json(a.flags)::text like '%%boxentry%%' then 1 else 0 end) as successful_passes_into_box
  --run quality
  , sum(case when a.eventname='reception' and b."type"='throughball' then 1 else 0 end) as throughball_receptions
  , sum(case when a.eventname='reception' and flags2json(b.flags)::text like '%%Line%%' then 1 else 0 end) as lb_pass_receptions
  --goals, shots and assists
  , sum(case when a.eventname='goal' and a."type" !='own' then 1 else 0 end) as total_goals
  , sum(case when a.eventname='shot' and flags2json(a.flags)::text like '%%shotongoal%%' then 1 else 0 end) as shots_on_target
  , sum(case when flags2json(a.flags)::text like '%%"assist%%' then 1 else 0 end) as assists
  , sum(case when flags2json(a.flags)::text like '%%"shotassist%%' then 1 else 0 end) as shot_assists
  --penalties
  , sum(case when a.eventname='goal' and flags2json(b.flags)::text like '%%penalty%%' then 1 else 0 end) as penalties_scored
  , sum(case when flags2json(a.flags)::text like '%%penalty%%' and flags2json(a.flags)::text like '%%shotoffgoal%%' then 1 else 0 end) +
   sum(case when flags2json(a.flags)::text like '%%penalty%%' and c.eventname='save' then 1 else 0 end) as penalties_missed
   --fouls, yellow cards and red cards
  , sum(case when a.eventname='goal' and a."type"='own' then 1 else 0 end) as own_goals
  , sum(case when a.eventname='infraction' and a.outcome='failed'and a."zone"='def' then 1 else 0 end) as fouls_in_own_third
  , sum(case when a.eventname='infraction' and a.outcome='failed'and a."zone"='mid' then 1 else 0 end) as fouls_in_middle_third
  , sum(case when a.eventname='infraction' and a.outcome='failed'and a."zone"='off' then 1 else 0 end) as fouls_in_final_third
  , sum(case when a.eventname='yellowcard' then 1 else 0 end) as yellow_cards
  , sum(case when a.eventname='penalty' and a."type"='redcard' then 1 else 0 end) as red_cards
  --takeons and takeonsagainst
  , sum(case when a.eventname='takeon' then 1 else 0 end) as total_takeons
  , sum(case when a.eventname='takeon' and a.outcome='successful' then 1 else 0 end) as successful_takeons
  , sum(case when a.eventname='takeonagainst' then 1 else 0 end) as total_takeonsagainst
  , sum(case when a.eventname='takeonagainst' and a.outcome='successful' then 1 else 0 end) as successful_takeonsagainst
  --saves by distance
  --include goals by opposition as shots on target for goalkeeper
  , case when a.playerid=shots_faced.opposingteamgoalieoniceid then shots_faced else 0 end as shots_faced
  , sum(case when a.eventname in ('save', 'claim') and a.outcome='successful' and b.eventname='shot' and flags2json(b.flags)::text like '%%shotongoal%%' then 1 else 0 end) as total_shots_saved
  , sum(case when a.eventname in ('save', 'claim') and a.outcome='successful' and b.eventname='shot' and flags2json(b.flags)::text like '%%penalty%%' then 1 else 0 end) as penalty_saves
  , sum(case when a.eventname in ('save', 'claim') and a.outcome='successful' and b.eventname='shot' and flags2json(b.flags)::text like '%%short%%' then 1 else 0 end) as short_shot_saves
  , sum(case when a.eventname in ('save', 'claim') and a.outcome='successful' and b.eventname='shot' and flags2json(b.flags)::text like '%%medium%%' then 1 else 0 end) as medium_shot_saves
  , sum(case when a.eventname in ('save', 'claim') and a.outcome='successful' and b.eventname='shot' and flags2json(b.flags)::text like '%%long%%' then 1 else 0 end) as long_shot_saves
  , sum(case when a.eventname='claim' then 1 else 0 end) as claims
  , sum(case when a.eventname='claim' and a.outcome='successful' then 1 else 0 end) as successful_claims
  from playereventlocations_dw a
  left join playereventlocations_dw b
  on a.secondaryplayereventid=b.playereventid
  and a.gameid=b.gameid
  left join
  subsequent_events c
  on a.playereventid=c.secondaryplayereventid
  --overall result
  left join scores
  on a.gameid=scores.gameid
  and a.teamshorthand=scores.team
  --player minutes*/
  left join play_time
  on a.gameid=play_time.gameid
  and a.playerid=play_time.playerid
  left join game_time
  on a.gameid=game_time.gameid
  left join shots_faced
  on a.gameid=shots_faced.gameid
  and a.playerid=shots_faced.opposingteamgoalieoniceid
  where a.gameid in (select gameid from games)
  group by 1,2,3,4,5,6,7,8,9,10,11,46;
'''


mailto = ['gabriel.stmaurice@sportlogiq.com', 'eimear@sportlogiq.com']
contents = 'Here is the latest SliQ Index'
subject = 'SliQ_index_{0}'.format(game_id)
file_name = 'sliq_index_{0}.csv'.format(game_id)

df = db.get_db_df(sql.format(game_id), (), dsn=os.getenv('SOCCER_URL'))
df.to_csv(file_name)

notifications.dispatch_notification(mailto, contents, subject, file_name, file_list=False)
